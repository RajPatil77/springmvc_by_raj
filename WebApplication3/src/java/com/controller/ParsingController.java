package com.controller;

import java.io.*;
import java.util.HashSet;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class ParsingController extends AbstractController {

    public final int threshold = 20;
    Set<String> Unique = new HashSet<String>();
    Set<String> Duplicates = new HashSet<String>();

    List<List<String>> Products = new ArrayList<>();
    List<String> leven = new ArrayList<>();

    //Levinshtein function
    public static int distance(String a, String b) {
        String lowa = a.toLowerCase();
        String lowb = b.toLowerCase();
        String[] splitlowa = lowa.split(",");
        String[] splitlowb = lowb.split(",");
        a = Arrays.toString(splitlowa);
        b = Arrays.toString(splitlowb);
        // i == 0
        int[] costs = new int[b.length() + 1];
        for (int j = 0; j < costs.length; j++) {
            costs[j] = j;
        }
        for (int i = 1; i <= a.length(); i++) {
            // j == 0; nw = lev(i - 1, j)
            costs[0] = i;
            int nw = i - 1;
            for (int j = 1; j <= b.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }
        return costs[b.length()];
    }

    //duplicacy check function
    public void removeDuplicates() {

        for (int i = 1; i < leven.size() - 1; i++) {
//      System.out.println(" comparing -  " + leven.get(i));
            Set<Integer> dup = new HashSet<Integer>();
            if (this.Duplicates.contains(leven.get(i))) {
//           System.out.println("galat = " + leven.get(i));
                continue;
            }
            for (int j = i + 1; j < leven.size(); j++) {

//                   hashcode.distance(leven.get(i), leven.get(j));
                int difference = ParsingController.distance(leven.get(i), leven.get(j));
                if (difference <= this.threshold) {
                    dup.add(j);
                }
                //calucalte levinshtein distance of i,j
                //if distance > 20, put in unique list
                //else populate duplicate list
            }
            if (dup.size() > 0) {
                this.Duplicates.add(leven.get(i));
                for (Integer k : dup) {

                    this.Duplicates.add(leven.get(k));

                }
            } else {
                this.Unique.add(leven.get(i));
            }
        }
    }

    //CSV file parser function
    public void readFile(String csvFileToRead) {
        BufferedReader br = null;
        String line = "";
        try {
            br = new BufferedReader(new FileReader(csvFileToRead));

            while ((line = br.readLine()) != null) {
                leven.add(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {

        //  ModelAndView mv = null;
        String csvFileToRead = "C:/Users/rd7pa/Desktop/validity/normal.csv";

        ParsingController object = new ParsingController();
        object.readFile(csvFileToRead);
        object.removeDuplicates();
        for (String uni : object.Unique) {
            System.out.println(uni);
        }
        Map<String, Object> m = new HashMap<String, Object>();

        m.put("unique", object.Unique);
        m.put("duplicates", object.Duplicates);

//        hsr.setAttribute("unique", object.Unique);
        System.out.println("++++++++++++++++++++");

        return new ModelAndView("success", "data", m);
    }
}

<%-- 
    Document   : success
    Created on : Apr 2, 2019, 1:17:07 AM
    Author     : rd7pa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:set var="Message" value="${requestScope.Message}"/>
        <h1>Welcome to Validity</h1>
        
        <c:forEach items="${requestScope.data}" var="item">
            <c:set var="key" value="${item.key}"/>
            <c:if test="${key eq 'unique'}" >
                <h1>Unique Entries: </h1>
                <c:forEach items="${item.value}" var="items">
                  <ul>${items}</ul>  
                </c:forEach> 
            </c:if>
                  
                  
            <c:if test="${ key eq 'duplicates'}" >
                <h1>Potentially Duplicate Entries: </h1>
                <c:forEach items="${item.value}" var="items">
                  <ul>${items}</ul>  
                </c:forEach> 
            </c:if>
             </c:forEach>
          
    </body>
</html>

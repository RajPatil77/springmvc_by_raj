# Spring MVC application

This is a simple web application in spring MVC framework

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes.

### Prerequisites

To run this application you need any browser installed on your system i.e. internet explorer,firefox mozilla, safari and an IDE for development. (I used NetBeans) 

### How to use

Download package and extract needed.

After extracting just go to extracted folder you will find all files.

you will find follwing files 

```
-	dist
-	build
-	nbproject
- 	src
-	test
-   Web
-   build
-	README.md
```

## How to run

extract this zip on desktop.
Open this extracted folder in IDE.
Right click project => project properties => libraries => select Spring Framework 4.0 and click "Import"
Setup server for deploymentif havent set any.
Simply right click the project name and select run.
Click browse file button and select file to parse.
Then click submit button and all the data will be displayed.

## Authors

**Raj Patil**

## Acknowledgement

Used this link to understand the Levenshtein distance concept - https://www.cuelogic.com/blog/the-levenshtein-algorithm

## Development server

Run server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.
